PP Docker
Dockert ne rootként futtassuk, ha lehet biztonsági okokból.
Sok konténer = orkesztrator: swarm (1-5 szerverig) vagy Kubernetes (más kontainereket is tud)
docker-machine (mi ez?) ezzel gépeket lehet létrehozni.

docker image ls //kilistázza az image-ekete
docker run -it ubuntu //Image indítása konténerben interaktívan
docker ps //listázza a konténereket
docker ps -a // a kilépetteket is listázza
docker rm $(docker ps -aq) //kitörli az összes konténert
docker rm -f $(docker ps -aq) //kitörli az összes konténert a futókat is

Volume
Ezt nem tudjuk, hogy hol van
docker volume create store //csinál egy store nevű volume-t
docker volume ls |grep store
docker run -it -v store:/store ubuntu bash//A store-t felcsatoljuk

Bind mountolás
docker run -it -v ${PWD}/store:/store ubuntu bash //ez hasonló, csak itt a könyvtárat osztjuk meg

Port beállítás
docker run -d -p 8081:80 php:apache //A host gép 8081-ét mapeljük a 80-as portra. loalhost:8081

docker exec -it 3c066fc6d6e5 bash // Belépünk egy futó kontainerbe

index.php
<?php phpinfo();

Dockerfile:
FROM php:apache
COPY index.php /var/www/html/index.php

docker build -t sajt/phpinfo .


——

docker inspect containerneve // megmutatja az ip címet is
docker container prune //kitörli az összes containert

docker network create testnet
docker run --rm -d --name phpinfo --network testnet sajt/phpinfo //container indítása testnet hálózaton

docker-composer up / down

docker-compose scale phpinfo=3
docker-composer ps
